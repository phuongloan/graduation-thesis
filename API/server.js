/**
 * Created by phuon on 5/13/2017.
 */
var mysql = require('mysql');
var express = require('express');
var bodyParser =    require("body-parser");
var schedule = require('node-schedule');
const PORT = process.env.PORT || 5001

var app = express();
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

//Register static assets
app.use('/assets', express.static('assets'));

var request = require("request");

var XLSX = require('xlsx');


var connection = mysql.createConnection({
    // host     : '127.0.0.1',
    // user     : 'root',
    // password : 'Loan2812',
    // database : 'siddb'

    host     : 'sql12.freesqldatabase.com',
    user     : 'sql12213236',
    password : 'Fn3pCLkRjG',
    database : 'sql12213236'
});
connection.connect(function(err) {
    if (err) console.log("Database Disconnected!", err);
    else console.log("Database Connected!");
});

const YahooFinanceAPI = require('yahoo-finance-data');

const api = new YahooFinanceAPI({
    key: 'dj0yJmk9bk9Xem5wTHNjRnEzJmQ9WVdrOVRUQTNhVXRvTjJrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1iNg--',
    secret: '8368222410a0b322ecd00c1f1a9e986975e964b2'
});

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    // res.setHeader('Access-Control-Allow-Origin', 'https://sidfinance-app.herokuapp.com');
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
var theMostActiveSymbol = 'CNIT';
var dayRanges=[];
var workbook = XLSX.readFile('assets/book1.xlsx');
var sheet_name_list = workbook.SheetNames;
var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
var totalSymbol = 2000;
// var totalSymbol = xlData.length;
var symbolTop500=[];
var symbolTop100=[];

var listSymbolActive=[]


function getSymbolMostActive() {
    var length = listSymbolActive.length;

    theMostActiveSymbol = listSymbolActive[0].symbol;
    console.log('theMostActiveSymbol', theMostActiveSymbol);
}

function getThemostActiveList() {
    var counter = 0;
    for(var i = 0; i< totalSymbol; i++){
        api
            .getHistoricalData(xlData[i].Ticker, '1m', '2d')
            .then(function(data){
                console.log('====================success', counter);

                if(data.chart.result[0]){
                    var record = {
                        openPrice : data.chart.result[0].indicators.quote[0].open[0],
                        close: data.chart.result[0].indicators.quote[0].close,
                        timestamp: data.chart.result[0].timestamp,
                        totalLength: data.chart.result[0].timestamp?data.chart.result[0].timestamp.length:0
                    }
                    counter++;

                    if(record.openPrice > 1){
                        while(record.close[record.totalLength-1] == null){
                            record.totalLength--;
                        }
                        var swing = 0;
                        var timeSwing = 0;
                        var dayHigh = record.close[record.totalLength-1];
                        var dayLow = record.close[record.totalLength-1];
                        var t = 2;
                        var symbol = {};
                        for (var j = 2; j < 390; j++){
                            if((record.close[record.totalLength-j] > record.close[record.totalLength-j-1]) && (record.close[record.totalLength-j] > record.close[record.totalLength-j+1])){
                                swing ++;
                                timeSwing = timeSwing + (j - t);
                                t = j;
                            }
                            if(record.close[record.totalLength-j] > dayHigh){
                                dayHigh = record.close[record.totalLength-j];
                            }else {
                                if(record.close[record.totalLength-j] < dayLow && record.close[record.totalLength-j] != null){
                                    dayLow = record.close[record.totalLength-j];
                                }
                            }
                        }
                        symbol.dayHigh = dayHigh;
                        symbol.dayLow = dayLow;
                        symbol.realSwing = swing;
                        symbol.time = timeSwing;
                        symbol.dayRange = ((dayHigh-dayLow)/record.openPrice)*0.3;
                        symbol.swing = (swing/195)*0.5;
                        symbol.timeSwing = ((timeSwing/swing)/390)*0.2;
                        symbol.totalPoint = symbol.dayRange + symbol.swing + symbol.timeSwing;
                        symbol.symbol = data.chart.result[0].meta.symbol;
                        listSymbolActive.push(symbol);
                        listSymbolActive = listSymbolActive.sort(compareValues('totalPoint','desc'));
                    }

                }
                if(counter == totalSymbol){
                    console.log('--------------------------------------------------------------------------------------------done top 500!!!');
                    getSymbolMostActive();
                    // getTop100();
                }
            })
            .catch( function(err){
                counter++;
                console.log('====================err', counter);

                if(counter == totalSymbol){
                    console.log('--------------------------------------------------------------------------------------------done top 500!!!');
                    // getTop100();
                    getSymbolMostActive();
                }

            });
    }
}

function compareValues(key, order) {
    return function(a, b) {
        if(!a.hasOwnProperty(key) ||
            !b.hasOwnProperty(key)) {
            return 0;
        }

        const varA = (a[key] === null) ?
            0 : a[key];
        const varB = (typeof b[key] === null) ?
            0 : b[key];

        var comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }
        return (
            (order == 'desc') ?
                (comparison * -1) : comparison
        );
    };
}

app.get('/home/getAll', function(req, res, next){
    // return res.send(listSymbolActive);
    api
        .getHistoricalData(theMostActiveSymbol, '1m', '1d')
        .then(function(data){
            return res.send(data);
        })
        .catch( function(err){
            return res.send(err);
        });
})

app.get('/searchBySymbol', function(req, res, next){
    api
        .getRealtimeQuotes(theMostActiveSymbol)
        .then(function(data){
            return res.send(data.query.results.quote);
        })
        .catch( function(err){
            return res.send(err);
        });
})
app.get('/historyBySymbol', function (req, res, next) {
        api
        .getHistoricalData(theMostActiveSymbol, '5m', '1mo')
        .then(function(data){
            return res.send(data);
        })
        .catch( function(err){
            return res.send(err);
        });
})
app.get('/tickerSearch', function (req, res, next) {
    var symbols = req.query.symbols;

    api
        .tickerSearch(symbols)
        .then(function(data){
            return res.send(data);
        })
        .catch( function(err){
            return res.send(err);
        });
})
app.get('/quoteSummary', function (req, res, next) {
    api
        .quoteSummary(theMostActiveSymbol)
        .then(function(data){
            return res.send(data);
        })
        .catch( function(err){
            return res.send(err);
        });
})
app.get('/recommendations', function (req, res, next) {
    var symbols = req.query.symbols;

    api
        .recommendations(theMostActiveSymbol)
        .then(function(data){
            return res.send(data);
        })
        .catch( function(err){
            return res.send(err);
        });
})
app.post('/saveDecision', function (req, res, next) {
    console.log('===========req', req.body);
    var cope = req.body;
    var dataUpdate={};
    var resultId;
    var query = connection.query('insert into result set ?', cope, function(err, result) {
        if (err) {
            console.error(err);
        } else {
            resultId = result.insertId;
        }
    });

    setTimeout(function () {
        api
            .getRealtimeQuotes(cope.symbol)
            .then(function(data){
                dataUpdate.futurePrice = data.query.results.quote.realtime_price;
                if(dataUpdate.futurePrice > cope.currPrice) dataUpdate.realChange = 1;
                if(dataUpdate.futurePrice < cope.currPrice) dataUpdate.realChange = 0;
                if(dataUpdate.futurePrice == cope.currPrice) dataUpdate.realChange = 2;
                if(cope.forecast == dataUpdate.realChange){
                    dataUpdate.finalStatus = true;
                }else{
                    dataUpdate.finalStatus = false;
                }

                var queryUpdate = connection.query("update result set ? where id="+ resultId, dataUpdate, function(err, result) {
                    if (err) {
                        console.error(err);
                        return res.send(err);
                    } else {
                        return res.send('Ok');
                    }
                })
            })
            .catch( function(err){
                console.error(err);
            });
    }, 300000)

})

app.get('/statistics', function (req, res, next) {
    var result={};
    result.list={}
    connection.query('SELECT * from result WHERE finalStatus = 1', function (err, rows, fields) {
        if (!err){
            result.right = rows.length;
            connection.query('SELECT * from result WHERE finalStatus = 0', function (err, rows, fields) {
                if (!err){
                    result.wrong = rows.length;
                    var count = 0;
                    for (var i = 0; i<21; i++){
                        connection.query('SELECT * from result WHERE decision'+i+' = 0', function (err, rows, fields) {
                            count++;
                            if (!err){
                                result.list[count-1] = rows.length;
                            }
                            else console.log(err);
                            if(count == 21){
                                return res.send(result);
                            }
                        });

                    }

                }
                else console.log(err);
            });
        }
        else console.log(err);
    });




})

app.listen(PORT, function (error) {
    if (error) {
        console.log('Unable to listen for connections', error)
        process.exit(10)
    }
    console.log('express is listening on http://' +
        PORT);

    setInterval(getThemostActiveList, 3600000);
    getThemostActiveList();
    var j = schedule.scheduleJob({hour: 04, minute: 00, dayOfWeek: [2,3,4,5,6]}, function(){
        console.log('Time for get symbol!');
        getThemostActiveList();
    });



})