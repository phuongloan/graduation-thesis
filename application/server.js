
// #!/usr/bin/env node
var app = require('./index')
var config = require('./_config')
const PORT = process.env.PORT || 3000

// Use whichever logging system you prefer.
// Doesn't have to be bole, I just wanted something more or less realistic
var bole = require('bole')

bole.output({level: 'debug', stream: process.stdout})
var log = bole('server')

log.info('server process starting')


// Note that there's not much logic in this file.
// The server should be mostly "glue" code to set things up and
// then start listening
app.listen(PORT, function (error) {
    if (error) {
        log.error('Unable to listen for connections', error)
        process.exit(10)
    }
    log.info('express is listening on http://' +
        PORT)
})
