/**
 * Created by phuon on 5/13/2017.
 */

(function() {
    angular
        .module("CoreModule", ['ngCookies'])
        .config(configHttpRequest)

    function configHttpRequest($httpProvider){
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
})();