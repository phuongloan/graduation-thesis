/**
 * Created by phuon on 5/13/2017.
 */

(function(){
    'use strict';
    angular
        .module("CoreModule",['ngCookies','ipCookie','infinite-scroll'])
        .factory("baseService", baseService);

    function baseService($http,$q){

        return {
            getBase : getBase,
            postBase : postBase
        };

        function getBase(api, params){
            var deferred = $q.defer();
            var url = config.api.URL + "/"+ api + "?" + params;
            $http.get(url,JSON.stringify(params)).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.resolve(response);
            });

            return deferred.promise;
        }

        function postBase(api, params) {
            var deferred = $q.defer();
            var url = config.api.URL + "/"+ api;
            $http.post(url,JSON.stringify(params), {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            ).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        }
    }
})();