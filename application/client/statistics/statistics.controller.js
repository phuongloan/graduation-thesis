/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("statisticsModule")
        .controller("StatisticsController", statisticsController);

    function statisticsController(StatisticsService) {
        var vm = this;

        vm.init = function () {
            StatisticsService.getStatistics().then(function (data) {
                console.log(data);
                vm.data = data.data;
                vm.success = vm.data.right;
                vm.fail = vm.data.wrong;
                vm.total = vm.success + vm.fail;
                vm.list = vm.data.list;

                $("#chartContainer").CanvasJSChart({
                    title: {
                        text: "Statistical forecasting the stock price",
                        fontSize: 28,
                        fontColor: "#FF8719",
                        padding: 40,
                        fontFamily: "'Helvetica Neue',sans-serif"
                    },
                    axisY: {
                        title: "Products in %"
                    },
                    legend :{
                        verticalAlign: "center",
                        horizontalAlign: "right"
                    },
                    data: [
                        {
                            type: "pie",
                            showInLegend: true,
                            toolTipContent: "{label} <br/> {y} %",
                            indexLabel: "{y} %",
                            dataPoints: [
                                { label: "Success",  y: vm.success/vm.total*100, legendText: "Success", color: "#09B216"},
                                { label: "Fail",    y: vm.fail/vm.total*100, legendText: "Fail", color: "#FF3227"}
                            ]
                        }
                    ]
                });

                for(var i = 0; i<21; i++){

                    $("#chart"+i).CanvasJSChart({
                        title: {
                            text: "Forecast by group "+i,
                            fontSize: 20,
                            fontColor: "#FF8719",
                            padding: 40,
                            fontFamily: "'Helvetica Neue',sans-serif"
                        },
                        axisY: {
                            title: "Products in %"
                        },
                        legend :{
                            verticalAlign: "center",
                            horizontalAlign: "right"
                        },
                        data: [
                            {
                                type: "pie",
                                showInLegend: true,
                                toolTipContent: "{label} <br/> {y} %",
                                indexLabel: "{y} %",
                                dataPoints: [
                                    { label: "Success",  y: vm.list[i]/vm.total*100, legendText: "Success", color: "#09B216"},
                                    { label: "Fail",    y: 100 - vm.list[i]/vm.total*100, legendText: "Fail", color: "#FF3227"}
                                ]
                            }
                        ]
                    });
                }

            })

        }
    }
})();