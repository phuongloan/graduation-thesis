/**
 * Created by phuon on 5/14/2017.
 */
(function(){
    'use strict';
    angular
        .module("statisticsModule")
        .factory("StatisticsService", statisticsService);

    function statisticsService(baseService){

        return {
            getStatistics: getStatistics
        };

        function getStatistics(){
            return baseService.getBase('statistics');
        }
    }
})();