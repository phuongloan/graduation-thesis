/**
 * Created by phuon on 4/22/2017.
 */
(function(){
    'use strict';
    angular
        .module("statisticsModule", ['ui.router','ngSanitize','CoreModule'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'statistics',
            url: '/',
            templateUrl: '/client/statistics/views/statistics.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();