/**
 * Created by phuon on 5/14/2017.
 */
var config = {};
config.protocol = 'http://';

config.api = {
    URL: 'http://127.0.0.1:5001'
    // URL: 'https://sidfinance-api.herokuapp.com'

}
config.queryURL = window.location.search.substring(1);
config.goto = function (link) {
    var host = window.location.host;
    var protocol = window.location.protocol;
    var toLink = protocol+'//'+ host + link;
    window.location.href = toLink;
}