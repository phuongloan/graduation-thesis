/**
 * Created by phuon on 5/30/2017.
 */
(function() {
    'use strict';
    angular
        .module("navbarModule")
        .factory("NavbarService", navbarService)
        .controller("NavbarController", navbarController);

    function navbarService(baseService) {
        return {
            tickerSearch: tickerSearch
        };

        function tickerSearch(symbol){
            return baseService.getBase('tickerSearch', 'symbols='+symbol);
        }
    }

    function navbarController($cookies, NavbarService) {
        var vm = this;
        vm.sortby = config.sortby;
        var query = config.queryURL;
        vm.init = function () {

        }
        vm.checkUser = function () {
            vm.cookieUserID = $cookies.getObject('userID');
            if(vm.cookieUserID  == '' || vm.cookieUserID  == undefined){
                return false;
            }
            else {
                vm.username = $cookies.getObject('userName');
                return true;
            }
        }
    }
})();