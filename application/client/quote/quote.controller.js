/**
 * Created by phuon on 5/14/2017.
 */
(function() {
    'use strict';
    angular
        .module("quoteModule")
        .controller("QuoteController", quoteController)
        // Optional configuration
        .config(['ChartJsProvider', function (ChartJsProvider) {
            // Configure all charts
            ChartJsProvider.setOptions({
                responsive: true,
                scales: {
                    xAxes: [{
                        ticks: {
                            autoSkip:true,
                            maxTicksLimit:20
                        },
                    }],
                },
                hover: {
                    mode: "label"
                },
            });
            // Configure all line charts
            ChartJsProvider.setOptions('line', {
                // showLines: false
            });
        }])

    function quoteController($scope, QuoteService,$filter, $cookies, $http,$window, $interval) {
        var vm = this;
        var quote = $window.location.search.replace('?', '');
        vm.data = {};
        vm.sale = 0;
        vm.buy = 0;
        vm.noPurchase =0;
        vm.showDecision = false;

        vm.series = ['Series A'];
        vm.datasetOverride = {
            backgroundColor: "rgba(9, 178, 22,0.3)"
        };

        vm.init = function () {
            // if($cookies.getObject('userID') == '' || $cookies.getObject('userID') == undefined){
            //     config.goto('/');
            // }


            QuoteService.searchBySymbol(quote).then(function (data) {
                console.log(data);
                vm.symbolPrice = data.data;
                vm.data.symbol =  quote;
                vm.data.currPrice = vm.symbolPrice.realtime_price;
            })
            QuoteService.quoteSummary(quote).then(function (data) {
                console.log(data);
                if(!data.data.error)
                vm.symbolInfo = data.data.quoteSummary.result[0].assetProfile;
            })

            $interval(function () {
                QuoteService.searchBySymbol(quote).then(function (data) {
                    vm.data.currPrice = data.data.realtime_price;
                })
            }, 2000);
        }

        vm.getChart1d = function(timeInterval ){
            vm.labels = [];
            vm.data[0] = [];
            var dateFormat = 'HH:mm';
            QuoteService.getIntradayChartData(vm.data.symbol, timeInterval, '1d').then(function (data) {
                vm.chartData1d = data.data.chart.result[0];
                var totalTimeStamp = vm.chartData1d.timestamp.length;
                for(var i = 0; i<totalTimeStamp; i++){
                    vm.chartData1d.timestamp[i] = $filter('date')(vm.chartData1d.timestamp[i]*1000, dateFormat, '+0');
                }
                vm.labels = vm.chartData1d.timestamp;
                vm.data[0] = vm.chartData1d.indicators.quote[0].close;
            })
        }
        vm.getChart5d = function(timeInterval ){
            vm.labels = [];
            vm.data[0] = [];
            var dateFormat = 'HH:mm EEE';
            QuoteService.getIntradayChartData(vm.data.symbol, timeInterval, '5d').then(function (data) {
                vm.chartData5d = data.data.chart.result[0];
                var totalTimeStamp = vm.chartData5d.timestamp.length;
                for(var i = 0; i<totalTimeStamp; i++){
                    vm.chartData5d.timestamp[i] = $filter('date')(vm.chartData5d.timestamp[i]*1000, dateFormat, '+0');
                }
                vm.labels = vm.chartData5d.timestamp;
                vm.data[0] = vm.chartData5d.indicators.quote[0].close;
            })
        }
        vm.getChart1mo = function(timeInterval ){
            vm.labels = [];
            vm.data[0] = [];
            var dateFormat = 'MMM dd';
            QuoteService.getIntradayChartData(vm.data.symbol, timeInterval, '1mo').then(function (data) {
                vm.chartData1mo = data.data.chart.result[0];
                var totalTimeStamp = vm.chartData1mo.timestamp.length;
                for(var i = 0; i<totalTimeStamp; i++){
                    vm.chartData1mo.timestamp[i] = $filter('date')(vm.chartData1mo.timestamp[i]*1000, dateFormat, '+0');
                }
                vm.labels = vm.chartData1mo.timestamp;
                vm.data[0] = vm.chartData1mo.indicators.quote[0].close;
            })
        }
        vm.getChart6mo = function(timeInterval ){
            vm.labels = [];
            vm.data[0] = [];
            var dateFormat = 'MMM dd, yy';
            QuoteService.getIntradayChartData(vm.data.symbol, timeInterval, '6mo').then(function (data) {
                vm.chartData6mo = data.data.chart.result[0];
                var totalTimeStamp = vm.chartData6mo.timestamp.length;
                for(var i = 0; i<totalTimeStamp; i++){
                    vm.chartData6mo.timestamp[i] = $filter('date')(vm.chartData6mo.timestamp[i]*1000, dateFormat, '+0');
                }
                vm.labels = vm.chartData6mo.timestamp;
                vm.data[0] = vm.chartData6mo.indicators.quote[0].close;
            })
        }
        vm.getChart1y = function(timeInterval ){
            vm.labels = [];
            vm.data[0] = [];
            var dateFormat = 'MMM dd, yy';
            QuoteService.getIntradayChartData(vm.data.symbol, timeInterval, '1y').then(function (data) {
                vm.chartData1y = data.data.chart.result[0];
                var totalTimeStamp = vm.chartData1y.timestamp.length;
                for(var i = 0; i<totalTimeStamp; i++){
                    vm.chartData1y.timestamp[i] = $filter('date')(vm.chartData1y.timestamp[i]*1000, dateFormat, '+0');
                }
                vm.labels = vm.chartData1y.timestamp;
                vm.data[0] = vm.chartData1y.indicators.quote[0].close;
            })
        }

        vm.search = function(){
            QuoteService.historyBySymbol(quote, vm.data.time).then(function (data) {

                console.log('data history', data);

                vm.historyPrice = data.data.chart.result[0].indicators.quote[0];
                vm.totalLength = vm.historyPrice.close.length;

                while(vm.historyPrice.close[vm.totalLength-1] == null){
                    vm.totalLength --;
                }
                vm.historyPrice.close = vm.historyPrice.close.slice(0, vm.totalLength);
                vm.historyPrice.high = vm.historyPrice.high.slice(0, vm.totalLength);
                vm.historyPrice.low = vm.historyPrice.low.slice(0, vm.totalLength);

                var list20 = vm.historyPrice.close.slice(vm.totalLength - 20, vm.totalLength);
                var list50 = vm.historyPrice.close.slice(vm.totalLength - 50, vm.totalLength);
                console.log('close', vm.historyPrice.close, list20);
                vm.SMA20 = SMA(20, list20);
                vm.SMA50 = SMA(50, list50);
                console.log('SMA', vm.SMA20, vm.SMA50);
                vm.data.SMAdecision = SMAdecision(vm.SMA20, vm.symbolPrice.realtime_price, vm.historyPrice.close[vm.totalLength-1]);

                vm.listMACD = getListMACD(vm.historyPrice.close);
                vm.signalMACD = EMA(vm.listMACD, 8, 9);
                console.log('vm.signalMACD',vm.signalMACD);
                vm.data.EMAdecision1 = EMAdecision1(vm.listMACD[1], vm.listMACD[0]);
                vm.data.EMAdecision2 = EMAdecision2(vm.listMACD[1], vm.listMACD[0], vm.signalMACD);

                vm.data.momentumDecision = momentumDecision(vm.symbolPrice.realtime_price, vm.historyPrice.close[vm.totalLength-1], vm.historyPrice.close[vm.totalLength-2]);
                console.log('price', vm.symbolPrice.realtime_price, vm.historyPrice.close[vm.totalLength-1]);

                vm.data.bollingerDecision = bollingerDecision(vm.SMA20, vm.SMA50, vm.data.currPrice, vm.historyPrice.close[vm.totalLength-1]);

                vm.data.RSIdecision = RSIdecision(caculatorRSI(0), caculatorRSI(1));

                vm.getDI = caculatorDI(caculatorATR());
                vm.data.ADXdecision = ADXdecision(vm.getDI, DMIdecision(vm.getDI) );

                getFinalDecision();
                vm.showDecision = true;
                // QuoteService.saveDecision(vm.data).then(function (response) {
                //     console.log('response', response);

                // })
            })

            // QuoteService.getIntradayChartData(quote, vm.time).then(function (data) {
            //     console.log('intraday' ,data.data);
            // })
        }

        function EMA(list,counter, n) {     //counter == n-1
            var emaValue = 0;
            if(counter < 0){
                return emaValue;
            }
            else {
                if(counter == 0){
                    emaValue = list[counter];
                }
                else {
                    emaValue = list[counter] * 2 / (n +1) + EMA(list,counter-1, n) *(1 - 2 / (n+1));
                }

            }
            return emaValue;
        }

        function getListMACD(list){
            var listMACD = [];
            var totalLengthList = list.length;

            for(var i = 0; i< 9; i ++){
                listMACD.push(EMA(list.slice(totalLengthList - 12-i, totalLengthList - i), 11, 12) - EMA(list.slice(totalLengthList - 26- i, totalLengthList -i), 25, 26));
                console.log('list', listMACD);
            }
            return listMACD;
        }

        // function EMAdecision1(MACD){              //return true == buy; false == sale
        //     if(MACD > 0){
        //         vm.buy +=1;
        //         return 1;
        //     }else{
        //         vm.sale +=1;
        //         return 0;
        //     }
        // }
        function EMAdecision1(prevMACD, MACD){              //return true == buy; false == sale
            if(MACD >= 0 && prevMACD<MACD){
                vm.buy +=1;
                return 1;
            }else if(MACD <= 0 && prevMACD>MACD){
                vm.sale +=1;
                return 0;
            }
            else {
                vm.noPurchase +=1;
                return 2;
            }
        }
        // function EMAdecision2(MACD, signalMACD){              //return true == buy; false == sale
        //     if(MACD > signalMACD){
        //         vm.buy +=1;
        //         return 1;
        //     }else{
        //         vm.sale +=1;
        //         return 0;
        //     }
        // }
        function EMAdecision2(prevMACD, MACD, signalMACD){              //return true == buy; false == sale
            if(MACD > signalMACD && prevMACD<MACD){
                vm.buy +=1;
                return 1;
            }else if(MACD < signalMACD && prevMACD>MACD){
                vm.sale +=1;
                return 0;
            } else {
                vm.noPurchase +=1;
                return 2;
            }
        }

        function SMA(n, list){
            var totalLenght = list.length;
            var totalValue = 0;
            for(var i = 1; i<=n; i++){
                totalValue = totalValue + list[totalLenght-i];
            }
            return totalValue/n;
        }

        // function SMAdecision(sma20, price){              //return true == buy; false == sale
        //     if(sma20>sma50 || price > sma20 || price > sma50){
        //         vm.buy +=1;
        //         return 1;
        //     }else{
        //         vm.sale +=1;
        //         return 0;
        //     }
        // }
        function SMAdecision(sma20, price, prevprice){              //return true == buy; false == sale
            if( price >= sma20 && prevprice < sma20){
                vm.buy +=1;
                return 1;
            }
            else if( price <= sma20 && prevprice > sma20){
                vm.sale +=1;
                return 0;
            }
            else {
                vm.noPurchase +=1;
                return 2;
            }
        }
        function bollingerDecision(sma20, sma50, price, prevPrice){                //return true == buy; false == sale
            console.log('standardDeviation', standardDeviation(20));
            var priceHigh = sma20 + 2*standardDeviation(20);
            var priceLow = sma50 - 2*standardDeviation(20);
            if(price <= prevPrice && price <= priceLow){
                vm.buy +=1;
                return 1;
            }else{
                if(price >= prevPrice && price >= priceHigh){
                    vm.sale +=1;
                    return 0;
                }
                else {
                    vm.noPurchase +=1;
                    return 2;
                }

            }
        }

        function standardDeviation(n) {
            var sumTotal = 0;
            var avgTotal = 0;
            var sumDeviations = 0;

            for(var i = 0; i < n; i ++){
                sumTotal += vm.historyPrice.close[vm.totalLength-1-i];
            }
            avgTotal = sumTotal / n;

            for( var i = 0; i< n; i++){
                sumDeviations = sumDeviations + (avgTotal - vm.historyPrice.close[vm.totalLength-1-i])*(avgTotal - vm.historyPrice.close[vm.totalLength-1-i]);
            }

            return Math.sqrt(sumDeviations/n);
        }

        function momentumDecision(price, prevPrice, prev2Price){              //return true == buy; false == sale
            var momentum1 = price - prevPrice;
            var momentum2 = prevPrice - prev2Price;
            if(momentum1>0 && momentum1 >= momentum2){
                vm.buy +=1;
                return 1;
            }else{
                if(momentum1 < 0 && momentum1 < momentum2){
                    vm.sale +=1;
                    return 0;
                }

                else {
                    vm.noPurchase +=1;
                    return 2;
                }
            }
        }

        // function RSIdecision() {
        //     var priceIncrease = 0;
        //     var priceDecrease = 0;
        //     var RS, RSI;
        //     for(var i = 0; i< 14; i++){
        //         var change = vm.historyPrice.close[vm.totalLength-1-i] - vm.historyPrice.close[vm.totalLength-2-i];
        //         if(change >= 0) priceIncrease += change;
        //         else priceDecrease = priceDecrease + (0- change);
        //         console.log('priceIncrease', priceIncrease);
        //         console.log('priceDecrease', priceDecrease,vm.totalLength );
        //     }
        //     RS = priceIncrease/priceDecrease;
        //     RSI = 100 - 100/(1+RS);
        //     console.log('RSI',priceIncrease, priceDecrease, RS, RSI );
        //     if(RSI > 50){
        //         vm.buy +=1;
        //         return 1;
        //     }
        //     else{
        //         vm.sale +=1;
        //         return 0;
        //     }
        // }
        function caculatorRSI(time_change) {
            var priceIncrease = 0;
            var priceDecrease = 0;
            var RS, RSI;
            for(var i = 0; i< 14; i++){
                var change = vm.historyPrice.close[vm.totalLength-1-i-time_change] - vm.historyPrice.close[vm.totalLength-2-i-time_change];
                if(change >= 0) priceIncrease += change;
                else priceDecrease = priceDecrease + (0- change);
                console.log('priceIncrease', priceIncrease);
                console.log('priceDecrease', priceDecrease,vm.totalLength );
            }
            RS = priceIncrease/priceDecrease;
            RSI = 100 - 100/(1+RS);
            console.log('RSI',priceIncrease, priceDecrease, RS, RSI );
            return RSI;
        }

        function RSIdecision(RSI_cur, RSI_last) {
            if(RSI_last <30 && RSI_cur>=30 ){
                 vm.buy +=1;
                return 1;
             }
            else if (RSI_last>70 && RSI_cur<=70){
                 vm.sale +=1;
                 return 0;
             }
             else{
                    vm.noPurchase +=1;
                    return 2;
            }
        }
        function caculatorATR(){
            var TR = [];
            var totalTR14 = 0;
            var ATR = 0;
            for(var i=0; i<14; i++){
                var pricedeff_1= vm.historyPrice.high[vm.totalLength-1-i]-vm.historyPrice.low[vm.totalLength-1-i];
                var pricedeff_2= vm.historyPrice.high[vm.totalLength-1-i]-vm.historyPrice.close[vm.totalLength-2-i];
                var pricedeff_3= vm.historyPrice.low[vm.totalLength-1-i]-vm.historyPrice.close[vm.totalLength-2-i];
                if(pricedeff_1 > pricedeff_2 && pricedeff_1 > pricedeff_3) TR[i] = pricedeff_1;
                else if(pricedeff_2 > pricedeff_1 && pricedeff_2 > pricedeff_3) TR[i] = pricedeff_2;
                else if(pricedeff_3 > pricedeff_1 && pricedeff_3 > pricedeff_1) TR[i] = pricedeff_3;
                totalTR14 = totalTR14 + TR[i];
            }
            if (ATR == 0) ATR = totalTR14/14;
            else ATR = (ATR *13 +TR[0])/14;
            return ATR;
        }
        
        function caculatorDI(ATR){
            var plusDM = [];
            var minusDM = [];
            var di = {
                plusDI : [],
                minusDI: []
            };
            var plusEMA = [];
            var minusEMA = [];
            var i = 0;
            for(i = 0;i<14; i++){
                var UpMove = vm.historyPrice.high[vm.totalLength-1-(14-i)] - vm.historyPrice.high[vm.totalLength-2-(14-i)];
                var DownMove = vm.historyPrice.low[vm.totalLength-1-(14-i)]-vm.historyPrice.low[vm.totalLength-2-(14-i)];
                if (UpMove > DownMove && UpMove > 0) plusDM[i] = UpMove;
                 else plusDM[i] = 0;
                if (DownMove > UpMove && DownMove > 0) minusDM[i] = DownMove;
                 else minusDM[i] = 0;
                if (i==0) plusEMA[i] = vm.historyPrice[vm.totalLength-1-(14-i)];
                 else plusEMA[i] = plusEMA[i-1] + (vm.historyPrice[vm.totalLength-1-(14-i)] - plusEMA[i-1])* (plusDM[i]/ATR);
                if (i==0) minusEMA[i] = vm.historyPrice[vm.totalLength-1-i];
                 else minusEMA[i] = minusEMA[i-1] + (vm.historyPrice[vm.totalLength-1-(14-i)] - minusEMA[i-1])* (minusDM[i]/ATR);
                di.plusDI[i] = 100 * plusEMA[i];
                di.minusDI[i] = 100 * minusEMA[i];
            }
            return di;
        }

        function DMIdecision(di){
             if (di.plusDI[13] > di.plusDI[12] && di.plusDI[12] > di.minusDI[12]) return 1;
            else if (di.minusDI[13] < di.minusDI[12] && di.minusDI[13] < di.plusDI[13]) return 0;
            else return 2;
        }

        function ADXdecision(di, dmi){
            var emaofADX = [];
             for(var i=0; i<14; i++){
                if (i==0) emaofADX[i] = vm.historyPrice[vm.totalLength-1-(14-i)];
                 else emaofADX[i] = emaofADX[i-1] + (vm.historyPrice[vm.totalLength-1-(14-i)] - emaofADX[i-1])* (Math.abs(di.plusDI[13]-di.minusDI[13])/(di.plusDI[13]+di.minusDI[13]));
             }
            if (100*emaofADX < 20 || dmi == 2) {
                vm.noPurchase +=1;
                 return 2;
            }
            else{
                if(dmi == 1){
                    vm.buy +=1;
                    return 1;
                }
                if (dmi == 0) {
                    vm.sale +=1;
                    return 0;
                }
            }
        }

        function getFinalDecision() {
            if(vm.buy >= vm.sale && vm.buy >= vm.noPurchase){
                vm.data.finalDecision = 1;
            }
            else {
                if(vm.sale > vm.buy && vm.sale >= vm.noPurchase)
                    vm.data.finalDecision = 0;
                if(vm.noPurchase > vm.sale && vm.noPurchase > vm.buy)
                    vm.data.finalDecision = 2;
            }
        }

        
        
    }
})();
