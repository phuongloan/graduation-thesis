/**
 * Created by phuon on 4/22/2017.
 */
(function(){
    'use strict';
    angular
        .module("quoteModule", ['ui.router','CoreModule','chart.js'])
        .config(configState);

    function configState($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        var mainState = {
            name: 'quote',
            url: '/',
            templateUrl: '/client/quote/detail/views/quoteDetail.html',
            cache: false
        };
        $stateProvider.state(mainState);
    }
})();