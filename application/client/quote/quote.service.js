/**
 * Created by phuon on 5/14/2017.
 */
(function(){
    'use strict';
    angular
        .module("quoteModule")
        .factory("QuoteService", quoteService);

    function quoteService(baseService){

        return {
            searchBySymbol: searchBySymbol,
            quoteSummary: quoteSummary,
            historyBySymbol: historyBySymbol,
            saveDecision : saveDecision,
            getIntradayChartData: getIntradayChartData
        };

        function searchBySymbol(symbol){
            return baseService.getBase('searchBySymbol', 'symbols='+symbol);
        }
        function quoteSummary(symbol){
            return baseService.getBase('quoteSummary', 'symbols='+symbol);
        }
        function historyBySymbol(symbol, time){
            var query = {
                symbols: symbol,
                time: time
            }
            return baseService.getBase('historyBySymbol', 'query='+JSON.stringify(query));
        }
        function saveDecision(data) {
            return baseService.postBase('saveDecision', data);
        }
        function getIntradayChartData(symbol, timeInterval, time){
            var query = {
                symbols: symbol,
                time: time,
                timeInterval: timeInterval
            }
            return baseService.getBase('getIntradayChartData', 'query='+JSON.stringify(query));
        }
    }
})();