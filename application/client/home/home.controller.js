/**
 * Created by phuon on 5/13/2017.
 */
(function() {
    'use strict';
    angular
        .module("homeModule")
        .controller("homeController", homeController)

    function homeController($scope, HomeService, $timeout, $filter, $interval) {
        var vm = this;
        vm.data = {};
        vm.sale = 0;
        vm.buy = 0;
        vm.noPurchase =0;
        vm.hourOpenUSMaket = 'US Markets are closed';
        vm.closed = true;
        vm.historyForecast=[];
        var dateFormat = 'HH:mm';

        vm.init = function () {
            vm.options = {
                chart: {
                    type: 'candlestickBarChart',
                    height: 450,
                    margin : {
                        top: 20,
                        right: 20,
                        bottom: 66,
                        left: 60
                    },
                    x: function(d){ return d['date']; },
                    y: function(d){ return d['close']; },
                    duration: 0,

                    xAxis: {
                        axisLabel: 'Times',
                        tickFormat: function(d) {
                            return $filter('date')(d*1000, dateFormat, '-5');
                        },
                        showMaxMin: false
                    },

                    yAxis: {
                        axisLabel: 'Stock Price',
                        tickFormat: function(d){
                            return '$' + d3.format(',.2f')(d);
                        },
                        showMaxMin: false
                    },
                }
            };

            vm.dataChart = [{values: []}];
            vm.symbolInfo = {};

            vm.tableName = "Getting symbol ...";
            vm.time = new Date;
            vm.timeUTC = $filter('date')(vm.time, 'yyyy-MM-dd HH:mm:ss', '+7');
            vm.timeLocal = $filter('date')(vm.time, 'yyyy-MM-dd HH:mm:ss' , '-5');

            vm.quoteSummary();
            vm.searchBySymbol();

            getdataInterval();

            $interval(function () {
                vm.time = new Date;
                vm.timeUTC = $filter('date')(vm.time, 'yyyy-MM-dd HH:mm:ss', '+7');
                vm.timeLocal = $filter('date')(vm.time, 'yyyy-MM-dd HH:mm:ss' , '-5');

                //check open hour of US market
                var dayName = new Date(vm.timeLocal).getDay();
                var dayHour = new Date(vm.timeLocal).getHours();
                if(dayName >0 && dayName <6){
                    if(dayHour >= 9 && dayHour < 16){
                        vm.closed = false;
                        vm.hourOpenUSMaket = $filter('date')(vm.time, 'HH:mm' , '-5');
                        // vm.hourOpenUSMaket = $filter('date')(new Date(vm.time.getTime() + (5*60*1000)), 'HH:mm' , '-5');
                        getPriceRealtime();
                    }
                    else vm.closed = true;
                }else vm.closed =true;
            }, 2000);

            $interval(function () {
                if(!vm.closed){
                    getdataInterval();
                    vm.search();
                }
            }, 60000);
        }
        vm.quoteSummary = function(){
            HomeService.quoteSummary().then(function (data) {
                vm.symbolInfo = data.data.quoteSummary.result[0].assetProfile;
                console.log('summary', data);
            })
        }
        vm.searchBySymbol = function () {
            HomeService.searchBySymbol().then(function (data) {
                vm.symbol = data.data.Symbol;
                vm.tableName = data.data.Symbol + ' - ' + data.data.Name;
                vm.currPrice = data.data.realtime_price;
                vm.currChange = data.data.realtime_change;
                vm.currChangePercent = data.data.realtime_chg_percent;

            })
        }

        function getdataInterval() {
            HomeService.getAllHome().then(function (data) {
                console.log('data', data);

                if(data.data.error){
                    vm.error = 'Can not connect to API. Please refresh website!';
                }else {
                    vm.defaultData = data.data.chart.result[0];
                    vm.totalLength = vm.defaultData.timestamp.length;

                    var dataItem = {};
                    // if(totalLength > 60) totalLength = 60;
                    while(vm.defaultData.indicators.quote[0].open[vm.totalLength-1] == null){
                        vm.totalLength --;
                    }
                    var length = 60;
                    if(vm.totalLength<60) length = vm.totalLength;

                    for (var i = length; i > 0; i--){
                        dataItem = {
                            "date": vm.defaultData.timestamp[vm.totalLength-i],
                            "open": vm.defaultData.indicators.quote[0].open[vm.totalLength-i],
                            "high": vm.defaultData.indicators.quote[0].high[vm.totalLength-i],
                            "low": vm.defaultData.indicators.quote[0].low[vm.totalLength-i],
                            "close": vm.defaultData.indicators.quote[0].close[vm.totalLength-i]
                        };
                        vm.dataChart[0].values.push(dataItem);
                        if(vm.dataChart[0].values.length > 60){
                            vm.dataChart[0].values.shift();
                            console.log('length list', vm.dataChart[0].values.length);
                        }
                    }
                }
            })
        }

        function getPriceRealtime() {
            HomeService.searchBySymbol().then(function (data) {
                if(!data.error){
                    vm.currPrice = data.data.realtime_price;
                    vm.currChange = data.data.realtime_change;
                    vm.currChangePercent = data.data.realtime_chg_percent;
                }
            })

        }

        vm.search = function(){
            HomeService.historyBySymbol().then(function (data) {
                vm.historyPrice = data.data.chart.result[0].indicators.quote[0];
                vm.totalLength = vm.historyPrice.close.length;
                vm.resultForecast = {
                    'time': vm.hourOpenUSMaket,
                    'fullTime': $filter('date')(new Date(vm.time.getTime() + (5*60*1000)), 'yyyy-MM-dd HH:mm' , '-5'),
                    'symbol': vm.symbol,
                    'currPrice': vm.currPrice
                };
                vm.decisions = []

                while(vm.historyPrice.close[vm.totalLength-1] == null){
                    vm.totalLength --;
                }
                vm.historyPrice.close = vm.historyPrice.close.slice(0, vm.totalLength);
                vm.historyPrice.high = vm.historyPrice.high.slice(0, vm.totalLength);
                vm.historyPrice.low = vm.historyPrice.low.slice(0, vm.totalLength);

                var list20 = vm.historyPrice.close.slice(vm.totalLength - 20, vm.totalLength);
                var list50 = vm.historyPrice.close.slice(vm.totalLength - 50, vm.totalLength);
                console.log('close', vm.historyPrice.close, list20);

                vm.SMA20 = SMA(20, list20);
                vm.SMA50 = SMA(50, list50);
                // vm.data.SMAdecision = SMAdecision(vm.SMA20, vm.currPrice, vm.historyPrice.close[vm.totalLength-1]);
                vm.decisions.push(SMAdecision(vm.SMA20, vm.currPrice, vm.historyPrice.close[vm.totalLength-1]));

                vm.listMACD = getListMACD(vm.historyPrice.close);
                vm.signalMACD = EMA(vm.listMACD, 8, 9);
                // vm.data.EMAdecision1 = EMAdecision1(vm.listMACD[0]);
                vm.decisions.push(EMAdecision1(vm.listMACD[0]));
                // vm.data.EMAdecision1 = EMAdecision1(vm.listMACD[1], vm.listMACD[0]);
                // vm.data.EMAdecision2 = EMAdecision2(vm.listMACD[1], vm.listMACD[0], vm.signalMACD);
                vm.decisions.push(EMAdecision2(vm.listMACD[1], vm.listMACD[0], vm.signalMACD));

                vm.decisions.push(momentumDecision(vm.currPrice, vm.historyPrice.close[vm.totalLength-1], vm.historyPrice.close[vm.totalLength-2]));

                vm.decisions.push(bollingerDecision(vm.SMA20, vm.SMA50, vm.data.currPrice, vm.historyPrice.close[vm.totalLength-1]));
                // vm.data.bollingerDecision = bollingerDecision(vm.SMA20, vm.SMA50, vm.data.currPrice, vm.historyPrice.close[vm.totalLength-1]);

                vm.decisions.push(RSIdecision());
                // vm.data.RSIdecision = RSIdecision(caculatorRSI(0), caculatorRSI(1));

                vm.getDI = caculatorDI(caculatorATR());
                // vm.data.ADXdecision = ADXdecision(vm.getDI, DMIdecision(vm.getDI) );
                vm.decisions.push(ADXdecision(vm.getDI, DMIdecision(vm.getDI)));

                getFinalDecision();

                console.log('vm.listGroupDecision', vm.resultForecast);
                vm.showDecision = true;

                var forecast = {
                    'time': vm.hourOpenUSMaket,
                    'fullTime': $filter('date')(new Date(vm.time.getTime() + (5*60*1000)), 'yyyy-MM-dd HH:mm' , '-5'),
                    'forecast': vm.resultForecast.forecast,
                    'symbol': vm.symbol,
                    'currPrice': vm.currPrice
                }
                console.log('forecast', forecast);
                vm.historyForecast.push(forecast);
                //effect flash text
                function blinker() {
                    $('.blink').fadeOut(500);
                    $('.blink').fadeIn(500);
                }

                var blink = setInterval(blinker, 1000); //Runs every second
                $timeout(function () {
                    clearInterval(blink);
                }, 3000);

                if(vm.resultForecast.forecast != 2){
                    HomeService.saveDecision(vm.resultForecast).then(function (response) {
                        console.log('response', response);

                    });
                }

            })
        }

        function EMA(list,counter, n) {     //counter == n-1
            var emaValue = 0;
            if(counter < 0){
                return emaValue;
            }
            else {
                if(counter == 0){
                    emaValue = list[counter];
                }
                else {
                    emaValue = list[counter] * 2 / (n +1) + EMA(list,counter-1, n) *(1 - 2 / (n+1));
                }

            }
            return emaValue;
        }

        function getListMACD(list){
            var listMACD = [];
            var totalLengthList = list.length;

            for(var i = 0; i< 9; i ++){
                listMACD.push(EMA(list.slice(totalLengthList - 12-i, totalLengthList - i), 11, 12) - EMA(list.slice(totalLengthList - 26- i, totalLengthList -i), 25, 26));
                console.log('list', listMACD);
            }
            return listMACD;
        }

        // function EMAdecision1(prevMACD, MACD){              //return true == buy; false == sale
        //     if(MACD >= 0 && prevMACD<MACD){
        //         vm.buy +=1;
        //         return 1;
        //     }else if(MACD <= 0 && prevMACD>MACD){
        //         vm.sale +=1;
        //         return 0;
        //     }
        //     else {
        //         vm.noPurchase +=1;
        //         return 2;
        //     }
        // }

        function EMAdecision1(MACD){              //return true == buy; false == sale
            if(MACD > 0){
                vm.buy +=1;
                return 1;
            }else{
                vm.sale +=1;
                return 0;
            }
        }

        function EMAdecision2(prevMACD, MACD, signalMACD){              //return true == buy; false == sale
            if(MACD > signalMACD && prevMACD<MACD){
                vm.buy +=1;
                return 1;
            }else if(MACD < signalMACD && prevMACD>MACD){
                vm.sale +=1;
                return 0;
            } else {
                vm.noPurchase +=1;
                return 2;
            }
        }

        function SMA(n, list){
            var totalLenght = list.length;
            var totalValue = 0;
            for(var i = 1; i<=n; i++){
                totalValue = totalValue + list[totalLenght-i];
            }
            return totalValue/n;
        }

        function SMAdecision(sma20, price, prevprice){              //return true == buy; false == sale
            if( price >= sma20 && prevprice < sma20){
                vm.buy +=1;
                return 1;
            }
            else if( price <= sma20 && prevprice > sma20){
                vm.sale +=1;
                return 0;
            }
            else {
                vm.noPurchase +=1;
                return 2;
            }
        }

        function bollingerDecision(sma20, sma50, price, prevPrice){                //return true == buy; false == sale
            console.log('standardDeviation', standardDeviation(20));
            var priceHigh = sma20 + 2*standardDeviation(20);
            var priceLow = sma50 - 2*standardDeviation(20);
            if(price <= prevPrice && price <= priceLow){
                vm.buy +=1;
                return 1;
            }else{
                if(price >= prevPrice && price >= priceHigh){
                    vm.sale +=1;
                    return 0;
                }
                else {
                    vm.noPurchase +=1;
                    return 2;
                }

            }
        }

        function standardDeviation(n) {
            var sumTotal = 0;
            var avgTotal = 0;
            var sumDeviations = 0;

            for(var i = 0; i < n; i ++){
                sumTotal += vm.historyPrice.close[vm.totalLength-1-i];
            }
            avgTotal = sumTotal / n;

            for( var i = 0; i< n; i++){
                sumDeviations = sumDeviations + (avgTotal - vm.historyPrice.close[vm.totalLength-1-i])*(avgTotal - vm.historyPrice.close[vm.totalLength-1-i]);
            }

            return Math.sqrt(sumDeviations/n);
        }

        function momentumDecision(price, prevPrice, prev2Price){              //return true == buy; false == sale
            var momentum1 = price - prevPrice;
            var momentum2 = prevPrice - prev2Price;
            if(momentum1>0 && momentum1 >= momentum2){
                vm.buy +=1;
                return 1;
            }else{
                if(momentum1 < 0 && momentum1 < momentum2){
                    vm.sale +=1;
                    return 0;
                }

                else {
                    vm.noPurchase +=1;
                    return 2;
                }
            }
        }

        function caculatorRSI(time_change) {
            var priceIncrease = 0;
            var priceDecrease = 0;
            var RS, RSI;
            for(var i = 0; i< 14; i++){
                var change = vm.historyPrice.close[vm.totalLength-1-i-time_change] - vm.historyPrice.close[vm.totalLength-2-i-time_change];
                if(change >= 0) priceIncrease += change;
                else priceDecrease = priceDecrease + (0- change);
            }
            RS = priceIncrease/priceDecrease;
            RSI = 100 - 100/(1+RS);
            console.log('RSI',priceIncrease, priceDecrease, RS, RSI );
            return RSI;
        }

        function RSIdecision() {
            var priceIncrease = 0;
            var priceDecrease = 0;
            var RS, RSI;
            for(var i = 0; i< 14; i++){
                var change = vm.historyPrice.close[vm.totalLength-1-i] - vm.historyPrice.close[vm.totalLength-2-i];
                if(change >= 0) priceIncrease += change;
                else priceDecrease = priceDecrease + (0- change);
                console.log('priceIncrease', priceIncrease);
                console.log('priceDecrease', priceDecrease,vm.totalLength );
            }
            RS = priceIncrease/priceDecrease;
            RSI = 100 - 100/(1+RS);
            console.log('RSI',priceIncrease, priceDecrease, RS, RSI );
            if(RSI > 50){
                vm.buy +=1;
                return 1;
            }
            else{
                vm.sale +=1;
                return 0;
            }
        }

        // function RSIdecision(RSI_cur, RSI_last) {
        //     if(RSI_last <30 && RSI_cur>=30 ){
        //         vm.buy +=1;
        //         return 1;
        //     }
        //     else if (RSI_last>70 && RSI_cur<=70){
        //         vm.sale +=1;
        //         return 0;
        //     }
        //     else{
        //         vm.noPurchase +=1;
        //         return 2;
        //     }
        // }

        function caculatorATR(){
            var TR = [];
            var totalTR14 = 0;
            var ATR = 0;
            for(var i=0; i<14; i++){
                var pricedeff_1= vm.historyPrice.high[vm.totalLength-1-i]-vm.historyPrice.low[vm.totalLength-1-i];
                var pricedeff_2= vm.historyPrice.high[vm.totalLength-1-i]-vm.historyPrice.close[vm.totalLength-2-i];
                var pricedeff_3= vm.historyPrice.low[vm.totalLength-1-i]-vm.historyPrice.close[vm.totalLength-2-i];
                if(pricedeff_1 > pricedeff_2 && pricedeff_1 > pricedeff_3) TR[i] = pricedeff_1;
                else if(pricedeff_2 > pricedeff_1 && pricedeff_2 > pricedeff_3) TR[i] = pricedeff_2;
                else if(pricedeff_3 > pricedeff_1 && pricedeff_3 > pricedeff_1) TR[i] = pricedeff_3;
                totalTR14 = totalTR14 + TR[i];
            }
            if (ATR == 0) ATR = totalTR14/14;
            else ATR = (ATR *13 +TR[0])/14;
            return ATR;
        }

        function caculatorDI(ATR){
            var plusDM = [];
            var minusDM = [];
            var di = {
                plusDI : [],
                minusDI: []
            };
            var plusEMA = [];
            var minusEMA = [];
            var i = 0;
            for(i = 0;i<14; i++){
                var UpMove = vm.historyPrice.high[vm.totalLength-1-(14-i)] - vm.historyPrice.high[vm.totalLength-2-(14-i)];
                var DownMove = vm.historyPrice.low[vm.totalLength-1-(14-i)]-vm.historyPrice.low[vm.totalLength-2-(14-i)];
                if (UpMove > DownMove && UpMove > 0) plusDM[i] = UpMove;
                else plusDM[i] = 0;
                if (DownMove > UpMove && DownMove > 0) minusDM[i] = DownMove;
                else minusDM[i] = 0;
                if (i==0) plusEMA[i] = vm.historyPrice[vm.totalLength-1-(14-i)];
                else plusEMA[i] = plusEMA[i-1] + (vm.historyPrice[vm.totalLength-1-(14-i)] - plusEMA[i-1])* (plusDM[i]/ATR);
                if (i==0) minusEMA[i] = vm.historyPrice[vm.totalLength-1-i];
                else minusEMA[i] = minusEMA[i-1] + (vm.historyPrice[vm.totalLength-1-(14-i)] - minusEMA[i-1])* (minusDM[i]/ATR);
                di.plusDI[i] = 100 * plusEMA[i];
                di.minusDI[i] = 100 * minusEMA[i];
            }
            return di;
        }

        function DMIdecision(di){
            if (di.plusDI[13] > di.plusDI[12] && di.plusDI[12] > di.minusDI[12]) return 1;
            else if (di.minusDI[13] < di.minusDI[12] && di.minusDI[13] < di.plusDI[13]) return 0;
            else return 2;
        }

        function ADXdecision(di, dmi){
            var emaofADX = [];
            for(var i=0; i<14; i++){
                if (i==0) emaofADX[i] = vm.historyPrice[vm.totalLength-1-(14-i)];
                else emaofADX[i] = emaofADX[i-1] + (vm.historyPrice[vm.totalLength-1-(14-i)] - emaofADX[i-1])* (Math.abs(di.plusDI[13]-di.minusDI[13])/(di.plusDI[13]+di.minusDI[13]));
            }
            if (100*emaofADX < 20 || dmi == 2) {
                vm.noPurchase +=1;
                return 2;
            }
            else{
                if(dmi == 1){
                    vm.buy +=1;
                    return 1;
                }
                if (dmi == 0) {
                    vm.sale +=1;
                    return 0;
                }
            }
        }

        function getFinalDecision() {
            console.log('k_combinations', k_combinations(['sma', 'ema1', 'ema2','mmt', 'boll', 'rsi', 'adx' ],5));
            var listGroup = k_combinations(vm.decisions,5);

            for(var i = 0; i<21; i++){
                var buy = 0;
                var sale = 0;
                var hole = 0;
                var finalDecision;
                var group = listGroup[i];
                for(var j = 0; j<5; j++){
                    if(group[j] == 0){ sale++;}
                    else{
                        if(group[j] == 1) {buy++;}
                        else hole++;
                    }
                }
                console.log(buy, sale, hole);
                if(buy >= sale && buy >= hole){
                    finalDecision = 1;
                }
                else {
                    if(sale > buy && sale >= hole)
                        finalDecision = 0;
                    if(hole > sale && hole > buy)
                        finalDecision = 2;
                }
                vm.resultForecast['decision' + i] = finalDecision;
            }

            vm.resultForecast.forecast = vm.resultForecast.decision8;
            // if(vm.buy >= vm.sale && vm.buy >= vm.noPurchase){
            //     vm.data.finalDecision = 1;
            // }
            // else {
            //     if(vm.sale > vm.buy && vm.sale >= vm.noPurchase)
            //         vm.data.finalDecision = 0;
            //     if(vm.noPurchase > vm.sale && vm.noPurchase > vm.buy)
            //         vm.data.finalDecision = 2;
            // }

        }

        function k_combinations(set, k) {
            var i, j, combs, head, tailcombs;

            // There is no way to take e.g. sets of 5 elements from
            // a set of 4.
            if (k > set.length || k <= 0) {
                return [];
            }

            // K-sized set has only one K-sized subset.
            if (k == set.length) {
                return [set];
            }

            // There is N 1-sized subsets in a N-sized set.
            if (k == 1) {
                combs = [];
                for (i = 0; i < set.length; i++) {
                    combs.push([set[i]]);
                }
                return combs;
            }
            combs = [];
            for (i = 0; i < set.length - k + 1; i++) {
                // head is a list that includes only our current element.
                head = set.slice(i, i + 1);
                // We take smaller combinations from the subsequent elements
                tailcombs = k_combinations(set.slice(i + 1), k - 1);
                // For each (k-1)-combination we join it with the current
                // and store it to the set of k-combinations.
                for (j = 0; j < tailcombs.length; j++) {
                    combs.push(head.concat(tailcombs[j]));
                }
            }
            return combs;
        }


    }
})();