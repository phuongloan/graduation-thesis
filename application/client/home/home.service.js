/**
 * Created by phuon on 5/13/2017.
 */
(function(){
    'use strict';
    angular
        .module("homeModule")
        .factory("HomeService", homeService);

    function homeService(baseService){

        return {
            getAllHome: getAllHome,
            searchBySymbol: searchBySymbol,
            historyBySymbol: historyBySymbol,
            quoteSummary: quoteSummary,
            saveDecision : saveDecision,
        };

        function getAllHome(){
           return baseService.getBase('home/getAll');
        }
        function searchBySymbol(){
            return baseService.getBase('searchBySymbol');
        }
        function historyBySymbol(){
            return baseService.getBase('historyBySymbol');
        }
        function quoteSummary(symbol){
            return baseService.getBase('quoteSummary');
        }
        function saveDecision(data) {
            return baseService.postBase('saveDecision', data);
        }
    }
})();