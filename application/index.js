'use strict';
var express = require('express');
var engine = require('ejs-mate');
var helmet = require('helmet');

var app = express();
var bodyParser  = require('body-parser');

//Register view of each component
function registerViews (){
    var views = [];
    views.push(__dirname + "/components/_common/views");
    views.push(__dirname + "/components/home/views");
    views.push(__dirname + "/components/quote/views");
    views.push(__dirname + "/components/statistics/views");
    return views;
}

app.engine('ejs', engine);
app.set('views',registerViews());

// Register view engine
app.set('view engine', 'ejs');

//Protect app
app.use(helmet());

//Register json parser
app.use(bodyParser.json());

//Register static assets
app.use('/assets', express.static('assets'));
app.use('/client',express.static('client'));

// Load the routes ("controllers" -ish)
app.use(require('./components/home/router'));
app.use(require('./components/quote/router'));
app.use(require('./components/quote/router'));
app.use(require('./components/statistics/router'));

console.log(process.env.NODE_ENV);

// Export the app instance for unit testing via supertest
module.exports = app;




